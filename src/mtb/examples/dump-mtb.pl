#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use CBOR::XS;
use JSON;
use IPC::Open2;
use Getopt::Long;
use Pod::Usage;
use File::Temp qw/ tempfile /;
use Digest::SHA qw(hmac_sha256);
use MIME::Base64::URLSafe qw(urlsafe_b64decode);
use Encode qw(encode_utf8);
use MIME::Base64 qw(encode_base64);
use Convert::ASN1;
use Math::BigInt;

my $asn1sign = Convert::ASN1->new;
$asn1sign->prepare(q<

  SEQUENCE {
    r INTEGER,
    s INTEGER
  }

>);

my $asn1keypub = Convert::ASN1->new;
$asn1keypub->prepare(q<

  SEQUENCE {
    SEQUENCE {
      alg OBJECT IDENTIFIER,
      curve OBJECT IDENTIFIER
    }
    xy BIT STRING
  }

>);

my $alg = "1 2 840 10045 2 1";
my $curve  = "1.2.840.10045.3.1.7";
my $uncompressed = pack("H*", "04");

my $pubkeyfile = '';
my $devkeyfile = '';
my $kdkeyfile  = '';
my $debug      = 0;
my $help       = 0;

my $options = GetOptions(
    'help|?'   => \$help,
    'pubkey=s' => \$pubkeyfile,
    'devkey=s' => \$devkeyfile,
    'kdkey=s'  => \$kdkeyfile,
    'debug'    => \$debug
) or pod2usage(2);

pod2usage(1) if $help;

my $mtbfile = shift @ARGV;

if ( !$mtbfile ) { pod2usage(1); }

if ( !-e $mtbfile ) { die "Specified MTB file does not exist" }
if ( $pubkeyfile and !-e $pubkeyfile ) {
    die "Specified public key file does not exist";
}
if ( $devkeyfile and !-e $devkeyfile ) {
    die "Specified device key file does not exist";
}
if ( $kdkeyfile and !-e $kdkeyfile ) {
    die "Specified device key file does not exist";
}

# read data
my $container_cbor = "";
open( my $fh, "<", $mtbfile );
while (<$fh>) {
    $container_cbor .= $_;
}
close($fh);

################################################################################
# Decode container

my $container = decode_cbor($container_cbor);

my $version    = decode_cbor( $container->{'v'} );
my $outer_cbor = $container->{'p'};

printf( "Container version: %s\n", $version );
print "\n";

################################################################################
# Decode outer

my $outer_data = decode_cbor($outer_cbor);

die "Failed to decode outer CBOR structure" unless ($outer_data);

my $json = JSON->new->allow_nonref;

my $outer_header    = $outer_data->[0];
my $outer_payload   = $outer_data->[1];
my $outer_signature = $outer_data->[2];

my $outer = decode_cbor($outer_header);

die "Failed to decode outer CBOR header" unless ($outer_data);

if ( $outer->{'did'} ) {

    # If the 'did' object exists, this is an outer structure we are looking at.
    # Otherwise it is an inner structure, and the optional outer does not exist.

    printf( "Outer header: %s\n",
      $json->pretty->encode( decode_cbor($outer_header) ) );

    if ($debug) { 
      
        printf( "Outer payload (%d bytes): %s\n",
          length($outer_payload), unpack( "H*", $outer_payload ) );
      
        printf( "Outer signature (%d bytes): %s\n",
          length($outer_signature),
          unpack( "H*", $outer_signature )
        );

        print "\n";
    }

    if ($kdkeyfile or $devkeyfile) {

        my $key = "";

        # build header and append payload
        my @data = ();
        push @data, $outer_header;
        push @data, $outer_payload;

        # extract signer
        my $h   = decode_cbor($outer_header);
        my $kid = $h->{kid};
        my $did = $h->{did};
        printf( "kid=%s, did=%s\n", $kid, $did );


        if ($kdkeyfile) {

            my $kdk;

            open( $fh, "<", $kdkeyfile );
            while (<$fh>) {
                $kdk .= $_;
            }
            close($fh);

            my $didbin = urlsafe_b64decode($did);

            $kid = encode_utf8($kid);

            $key = substr hmac_sha256($didbin.$kid, $kdk), 0, 16;

            if ($debug) { 
            
                printf( "did: %s\n", unpack( "H*", $didbin ) );
                printf( "kid: %s\n", $kid );
                printf( "kdk: %s\n", unpack( "H*", $kdk ) );
                printf( "devkey: %s\n", unpack( "H*", $key ) );

            }

        } else {

            open( $fh, "<", $devkeyfile );
            while (<$fh>) {
                $key .= $_;
            }
            close($fh);
        }

        # sign header+payload
        my $outer_signature2 = hmac_sha256( encode_cbor( \@data ), $key );

        # check results
        if ( substr($outer_signature, 0, 16) eq substr($outer_signature2, 0, 16) ) {
            print "Outer signature validation successful\n";
        }
        else {
            print "Outer signature validation failed\n";
            
            if ($debug) { 
                printf( "Enclosed signature %s\n", 
                  unpack( "H*", $outer_signature ) );
                printf( "Computed signature %s\n", 
                  unpack( "H*", $outer_signature2 ) );
            }
        }
    }

} else {

    print "No outer structure\n";
		$outer_payload = $outer_cbor;

}

################################################################################
# Decode inner

my $inner_data = decode_cbor($outer_payload);

die "Failed to decode inner CBOR structure" unless ($inner_data);

my $inner_header    = $inner_data->[0];
my $inner_payload   = $inner_data->[1];
my $inner_signature = $inner_data->[2];


 printf( "Inner header: %s\n",
   $json->pretty->encode( decode_cbor($inner_header) ) );
 printf( "Inner payload: %s\n",
   $json->pretty->encode( decode_cbor($inner_payload) ) );

if ($debug) { 
    printf(
      "Inner signature (%d bytes): %s\n",
      length($inner_signature),
      unpack( "H*", $inner_signature )
    );

}


my $r_hex = unpack( "H*", substr( $inner_signature, 0, 32 ));
my $s_hex = unpack( "H*", substr( $inner_signature, 32, 63 ));

my $r = Math::BigInt->from_hex( $r_hex );
my $s = Math::BigInt->from_hex( $s_hex );

if ($debug) { 
  printf( "R (hex): %s\nS (hex): %s\n", $r_hex, $s_hex );
  printf( "R (int): %s\nS (int): %s\n", $r, $s);

}

if ($pubkeyfile) {

    # detect format 
    my $pubkey;
    open ( $fh, "<", $pubkeyfile );
    while (<$fh>) {
        chomp;
        $pubkey .= $_;
    }
    close($fh);
    
    if ( $pubkey =~ /-----BEGIN PUBLIC KEY-----/ ) {
        print "Public key in PEM format\n";
    } else {
        # converting jwk to pem
        my $jwk = $json->decode( $pubkey ) or
        die "Public key file can't be decoded in neither PEM or JWK format\n";
        
        print "Public key in JWK format\n";

        my $asn1struct = $asn1keypub->encode( 
            xy => $uncompressed.
                  urlsafe_b64decode($jwk->{x}).
                  urlsafe_b64decode($jwk->{y}), 
            alg => $alg,
            curve => $curve);
    
        my $fh2;
        ( $fh2, $pubkeyfile) = tempfile();
        print $fh2 "-----BEGIN PUBLIC KEY-----\n";
        print $fh2 encode_base64($asn1struct);
        print $fh2 "-----END PUBLIC KEY-----\n";
        close($fh2);
        
    }


    # build header and append payload
    my @data = ();
    push @data, $inner_header;
    push @data, $inner_payload;
    
    my $asn1struct = $asn1sign->encode( r => $r, s => $s);
    
    # write temporary data
    my ( $fh2, $signature_file ) = tempfile();
    print $fh2 $asn1struct;
    close($fh2);
    
    # verify header+payload
    my $pid = open2( *Reader, *Writer,
        "openssl dgst -sha256 -verify $pubkeyfile -signature $signature_file" );
    print Writer encode_cbor( \@data );
    close(Writer);
    
    if ($debug) { 
    
        printf( "Inner signature data (%d bytes): %s\n",
            length(encode_cbor( \@data )), 
            unpack( "H*", encode_cbor( \@data ) )
        );
        
        printf( "Inner signature asn1 (%d bytes): %s\n",
            length($asn1struct), 
            unpack( "H*", $asn1struct )
        );
    
    }
      
    my $output = <Reader>;
    waitpid( $pid, 0 );
    my $child_exit_status = $? >> 8;

    # extract signature
    my $h   = decode_cbor($inner_header);
    my $kid = $h->{kid};
    printf( "kid=%s\n", $kid );

    # check results
    if ( $child_exit_status == 0 ) {
        print "Inner signature validation successful\n";
    }
    else {
        print "Inner signature validation failed\n";
    }

    unlink($signature_file);

}

__END__
 
=head1 NAME

dump-mtb.pl - Example code for parsing a Mobile Ticket Blob
 
=head1 SYNOPSIS
 
dump-mtb.pl [options] [mtbfile]
 
 Options:
   --help           this help message
   --pubkey file    public key (PEM or JWK) file for inner signature verification
   --devkey file    device key file for outer signature verification
   --kdkey file     key derivation key to recreate device key
   --debug          print debug information
 
=head1 OPTIONS
 
=over 8
 
=item B<--help>
 
Print a brief help message and exits.
 
=item B<--pubkey>
 
If given an ECDSA public key file, attempt to validate the inner 
signature
 
=item B<--devkey>
 
If given a symmetric device key file, attempt to validate the outer
signature
 
=back
 
=head1 DESCRIPTION
 
B<This program> will read the mobile ticket blob given as the input 
file and dump the contents, provided it can be parsed. Optionally, 
if given a ECDSA public key file, it will attempt to validate the 
inner signature of the mobile ticket blob.

=cut


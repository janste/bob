#!/usr/bin/perl

use strict ;
use warnings ;
use IO::Compress::Deflate qw(deflate $DeflateError) ;

my $input = shift @ARGV;
my $output = $input . ".deflated";
deflate $input => $output
    or die "deflate failed: $DeflateError\n";

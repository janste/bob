#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use CBOR::XS;
use FileHandle;
use IPC::Open2;
use Convert::ASN1;

my $asn1sign = Convert::ASN1->new;
$asn1sign->prepare(q<

  SEQUENCE {
    r INTEGER,
    s INTEGER
  }

>);

my $key_file     = shift @ARGV;
my $header_file  = shift @ARGV;
my $payload_file = shift @ARGV;

my @data = ();

# read payload
my $payload = "";
open(my $fh, "<", $payload_file);
while (<$fh>) {
    $payload .= $_;
}
close($fh);

# read header
my $header = "";
open($fh, "<", $header_file);
while (<$fh>) {
    $header .= $_;
}
close($fh);

push @data, $header;
push @data, $payload;

# sign header+payload
my $pid = open2(*Reader, *Writer, "openssl dgst -sha256 -sign $key_file");
print Writer encode_cbor(\@data);
close(Writer);

my $signature;
while(<Reader>) {

 $signature .= $_;

}

my $rs = $asn1sign->decode($signature);
my $r_hex = pack "H*", Math::BigInt->new($rs->{r})->as_hex;
my $s_hex = pack "H*", Math::BigInt->new($rs->{s})->as_hex;

my $r = substr $r_hex, 1;
my $s = substr $s_hex, 1;

push @data, $r . $s;

# output as CBOR
print encode_cbor(\@data);

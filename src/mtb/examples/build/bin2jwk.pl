#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use FileHandle;
use IPC::Open2;
use MIME::Base64 qw(decode_base64);
use MIME::Base64::URLSafe qw(urlsafe_b64encode);

my $bin_file     = shift @ARGV;
my $jwk_file     = shift @ARGV;
my $fh;

my $keybin = "";
open($fh, "<", $bin_file);
while (<$fh>) {
    $keybin .= $_;
}
close($fh);

open(JWK, "> $jwk_file"); 

printf JWK ( "{\n" );
printf JWK ( "  \"kty\": \"oct\"\n" );
printf JWK ( "  \"k\": \"%s\",\n", urlsafe_b64encode($keybin) );
printf JWK ( "  \"kid\": \"%s\",\n", int(rand(9999)) );
printf JWK ( "  \"kty\": \"EC\"\n" );
printf JWK ( "}\n" );

close(JWK);


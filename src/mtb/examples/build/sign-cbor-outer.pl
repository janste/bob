#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use CBOR::XS;
use Digest::SHA qw(hmac_sha256);
use MIME::Base64::URLSafe qw(urlsafe_b64decode urlsafe_b64encode);
use Encode qw(encode_utf8);

my $key_file     = shift @ARGV;
my $header_file  = shift @ARGV;
my $payload_file = shift @ARGV;
my $devkey_file = shift @ARGV;
my $devkeyjwk_file = shift @ARGV;

my @data = ();
my $fh;

# read payload
my $payload = "";
open($fh, "<", $payload_file);
while (<$fh>) {
    $payload .= $_;
}
close($fh);

# read header
my $header = "";
open($fh, "<", $header_file);
while (<$fh>) {
    $header .= $_;
}
close($fh);

my $h = decode_cbor($header);
my $kid = $h->{kid};
my $did = $h->{did};

# read key derivation key
my $kdk;

open( $fh, "<", $key_file );
while (<$fh>) {
    $kdk .= $_;
}
close($fh);

my $didbin = urlsafe_b64decode($did);

$kid = encode_utf8($kid);
my $key = substr hmac_sha256($didbin.$kid, $kdk), 0, 16;

open( $fh, ">", $devkeyjwk_file );
printf $fh ( "{\n" );
printf $fh ( "  \"kty\": \"oct\"\n" );
printf $fh ( "  \"k\": \"%s\",\n", urlsafe_b64encode($key) );
printf $fh ( "  \"kid\": \"%s\",\n", $kid );
printf $fh ( "  \"kty\": \"EC\"\n" );
printf $fh ( "}\n" );
close($fh);

open( $fh, ">", $devkey_file );
printf $fh $key;
close($fh);

push @data, $header;
push @data, $payload;

# sign header+payload
my $signature = hmac_sha256(encode_cbor(\@data), $key);
my $truncated = substr $signature, 0, 16;
push @data, $truncated;

# output as CBOR
print encode_cbor(\@data);

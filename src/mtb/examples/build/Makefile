
VERSION=		version.json
PAYLOADDATA=		payloadData.json
DEVICESIGHEADER=	deviceSignatureHeader.json
ISSUERSIGHEADER=	issuerSignatureHeader.json

CBORVERSION=		version.json.cbor
CBORPAYLOADDATA=	payloadData.json.cbor
CBORDEVICESIGHEADER=	deviceSignatureHeader.json.cbor
CBORISSUERSIGHEADER=	issuerSignatureHeader.json.cbor

INNER=			signedTicketBundle.bin
OUTER=			signedDeviceBundle.bin

MTB=			mtb.bin
MTBDEF=			mtb.bin.deflated

PRIVATE=		key-private.pem
PRIVATEJWK=		key-private.json
PUBLIC=			key-public.pem
PUBLICJWK=		key-public.json
DEVKEY=			key-device.bin
DEVKEYJWK=		key-device.json
KDK=			kdk.bin
KDKJWK=			kdk.json
PEM2JWK=		$(PRIVATEJWK) $(PUBLICJWK)
KDK2JWK=		$(KDKJWK)

KEYFILES=		$(PRIVATE) $(PUBLIC) $(KDK)
CLEANFILES=		$(KEYFILES) $(CBORFILES) $(PEM2JWK) $(KDKJWK) $(DEVKEY) $(DEVKEYJWK)
CBORFILES=		$(CBORVERSION) $(CBORPAYLOADDATA) $(CBORDEVICESIGHEADER) $(CBORISSUERSIGHEADER) $(INNER) $(OUTER) $(MTB) $(MTBDEF)


all: $(KEYFILES) $(CBORFILES) $(PEM2JWK) $(KDK2JWK) $(INNER) $(OUTER) $(MTB) $(MTBDEF)

keys: $(KEYFILES)

cbor: $(CBORFILES)

inner: $(INNER)

outer: $(OUTER)

mtb: $(MTB)

$(CBORVERSION):
	@perl json2cbor.pl $(VERSION) > $(CBORVERSION)

$(CBORPAYLOADDATA):
	@perl json2cbor.pl $(PAYLOADDATA) > $(CBORPAYLOADDATA)
	
$(CBORDEVICESIGHEADER):
	@perl json2cbor.pl $(DEVICESIGHEADER) > $(CBORDEVICESIGHEADER)
	
$(CBORISSUERSIGHEADER):
	@perl json2cbor.pl $(ISSUERSIGHEADER) > $(CBORISSUERSIGHEADER)
	
$(PEM2JWK):
	@perl pem2jwk.pl $(PRIVATE) $(CBORDEVICESIGHEADER) $(PRIVATEJWK) $(PUBLICJWK)

$(KDK2JWK):
	@perl bin2jwk.pl $(KDK) $(KDKJWK)

$(PRIVATE):
	@openssl ecparam -genkey -name secp256r1 -noout -out $(PRIVATE)

$(PUBLIC): $(PRIVATE)
	@openssl ec -in $(PRIVATE) -pubout -out $(PUBLIC)

$(KDK):
	@openssl rand 32 > $(KDK)

$(INNER):
	@perl sign-cbor-inner.pl $(PRIVATE) $(CBORISSUERSIGHEADER) $(CBORPAYLOADDATA) > $(INNER)

$(OUTER): 
	@perl sign-cbor-outer.pl $(KDK) $(CBORDEVICESIGHEADER) $(INNER) $(DEVKEY) $(DEVKEYJWK) > $(OUTER)

$(MTB):
	@perl create-container.pl $(CBORVERSION) $(OUTER) > $(MTB)

$(MTBDEF):
	@perl deflate.pl $(MTB) > $(MTBDEF)

clean:
	rm -f $(CLEANFILES) 

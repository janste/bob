from imp_lexer import *
from combinators import *
from imp_ast import *
import sys

# Basic parsers
def keyword(kw):
    return Reserved(kw, RESERVED)

num = Tag(INT) ^ (lambda i: int(i))
myfloat = Tag(FLOAT) ^ (lambda f: float(f))
id = Tag(ID)
macro = Tag(MACRO)
prop = Tag(PROP)
tempus = Tag(TEMPUS)

# Top level parser
def imp_parse(tokens):
    if "test" in sys.argv:
        print "TOKENS",tokens
    ast = parser()(tokens, 0)
    if "test" in sys.argv:
        print "AST",ast
    return ast

def parser():
    return Phrase(stmt_list())    

# Statements
def stmt_list():
    separator = keyword(';') ^ (lambda x: lambda l, r: l + r)
    def process(stmts):
        return Statements([s for s in stmts if type(s) == Definition], [s for s in stmts if type(s) == Entitlement])
    return Exp(stmt(), separator) ^ process

def stmt():
    return (assign_stmt() | \
           decl_stmt()) ^ singleton

def assign_stmt():
    def process(parsed):
        ((name, _), exp) = parsed
        return Definition(name, exp)
    return macro + keyword(':=') + condition() ^ process

def decl_stmt():
    def process(parsed):
        ((name, _), exp) = parsed
        return Entitlement(name, exp)
    return id + keyword(':') + condition() ^ process

# Boolean expressions
def condition():
    return precedence(condition_term(),
                      condition_precedence_levels,
                      process_logic)

def condition_term():
    return condition_not()   | \
           condition_group() | \
           condition_prop()  | \
           condition_propList()  | \
           condition_tempus() | \
           condition_geoPoly() | \
           condition_geoRadius() | \
           condition_macro()

def condition_not():
    return keyword('!') + Lazy(condition_term) ^ (lambda parsed: Not(parsed[1]))

def condition_group():
    return keyword('(') + Lazy(condition) + keyword(')') ^ process_group

def condition_prop():
    def process(v):
        parts = v.split(":")
        return Property(parts[0],parts[1], [parts[2]])
    return prop ^ process

def id_list():
    process = lambda x: lambda l, r: l + r
    separator = keyword(',') ^ process
    return Exp(id ^ singleton, separator)

def condition_propList():
    def process(parsed):
        ((((((id, _), t), _), _), ids), _) = parsed
        return Property(id, t, ids)
    return id + keyword(':') + id + keyword(':') + keyword('[') + id_list() + keyword(']') ^ process

def coordinate_list():
    separator = keyword(',') ^ (lambda x: lambda l, r: CoordinateList(l, r))
    return Exp(coordinate(), separator)

def condition_geoPoly():
    def process(parsed):
        ((_, coordinates), _) = parsed
#        print "POLYGON:",coordinates
        return GeoArea(coordinates)
    return keyword('<<') + coordinate_list() + keyword('>>') ^ process

def condition_geoRadius():
    def process(parsed):
        ((coordinate, _), radius) = parsed
#        print "RADIUS:",coordinate,radius
        return GeoRadius(coordinate,radius)
    return coordinate() + keyword('/') +  myfloat ^ process

def coordinate():
    def process(parsed):
        ((((_, f1),_), f2), _) = parsed
#        print "COORDINATE:",f1,f2
        return (f1,f2)
    return keyword('{') + myfloat + keyword(',') + myfloat + keyword('}') ^ process

def condition_tempus():
    return (tempus ^ (lambda v : Time(v[1:])))

def condition_macro():
    return (macro ^ (lambda v : MacroReference(v)))

# Arithmetic expressions
def aexp():
    return precedence(aexp_term(),
                      aexp_precedence_levels,
                      process_binop)

def aexp_term():
    return aexp_value() | aexp_group()

def aexp_group():
    return keyword('(') + Lazy(aexp) + keyword(')') ^ process_group

def aexp_value():
    return (num ^ (lambda i: IntAexp(i))) | \
           (id  ^ (lambda v: VarAexp(v)))

# An IMP-specific combinator for binary operator expressions (aexp and condition)
def precedence(value_parser, precedence_levels, combine):
    def op_parser(precedence_level):
        return any_operator_in_list(precedence_level) ^ combine
    parser = value_parser * op_parser(precedence_levels[0])
    for precedence_level in precedence_levels[1:]:
        parser = parser * op_parser(precedence_level)
    return parser

# Miscellaneous functions for binary and relational operators
def process_binop(op):
    return lambda l, r: BinopAexp(op, l, r)

def process_relop(parsed):
    ((left, op), right) = parsed
    return RelopCondition(op, left, right)

def process_logic(op):
    if op == '&':
        return lambda l, r: And(l, r)
    elif op == '|':
        return lambda l, r: Or(l, r)
    else:
        raise RuntimeError('unknown logic operator: ' + op)

def process_group(parsed):
    ((_, p), _) = parsed
    return p

def process_other(parsed):
    print "OTHER:",parsed
    ((_, p), _) = parsed
    return p

def any_operator_in_list(ops):
    op_parsers = [keyword(op) for op in ops]
    parser = reduce(lambda l, r: l | r, op_parsers)
    return parser

# Operator keywords and precedence levels
aexp_precedence_levels = [
    ['*', '/'],
    ['+', '-'],
]

condition_precedence_levels = [
    ['&'],
    ['|'],
]

def singleton(x):
    return [x]

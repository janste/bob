from imp_ast import *

def switch(o, m):
    key, value = o.items()[0]
    return m[key](value)

def tickleFromJson(js):
    o = json.loads(js)
    return Statements(
        [Definition(s["name"], condition(s["condition"])) for s in o["c_ma"]],
        [Entitlement(s["pid"], condition(s["condition"])) for s in o["c_ta"]]
    )

def statement(o):
    return switch(o, {
        "definition":  lambda arg: Definition(arg["name"], condition(arg["condition"])),
        "entitlement": lambda arg: Entitlement(arg["pid"], condition(arg["condition"]))
    })

def coordinate(o):
    return (o["lat"], o["lon"])

def condition(o):
    return switch(o, {
        "or":        lambda args: reduce(Or, map(condition, args)),
        "and":       lambda args: reduce(And, map(condition, args)),
        "not":       lambda arg: Not(condition(arg)),
        "temporal":  lambda arg: Time(arg),
        "macro":     lambda arg: MacroReference(arg),
        "property":  lambda arg: Property(arg["pid"], arg["type"], arg["values"]),
        "geoRadius": lambda arg: GeoRadius(coordinate(arg["coordinate"]), arg["radius"]),
        "geoArea":   lambda arg: GeoArea(reduce(CoordinateList, map(coordinate, arg["coordinates"])))
    })
